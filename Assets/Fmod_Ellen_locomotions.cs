using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FMODUnity;

public class Fmod_Ellen_locomotions : MonoBehaviour
{
    public string footEeeevent;
    public string lilStepEvent;
    public string lilWhooshEvent;
    public string rollEvent;
    public string tapTapEvent;
    public string FishEvent;
    //[FMODUnity.EventRef] public string footEvent;
    FMOD.Studio.EventInstance eventInstance;



    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    void footstep()
    {
        eventInstance = RuntimeManager.CreateInstance(footEeeevent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
    void lilStep()
    {
        eventInstance = RuntimeManager.CreateInstance(lilStepEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
    void lilWhoosh()
    {
        eventInstance = RuntimeManager.CreateInstance(lilWhooshEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
    void roll()
    {
        eventInstance = RuntimeManager.CreateInstance(rollEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
    void tapTap()
    {
        eventInstance = RuntimeManager.CreateInstance(tapTapEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
    void Fish()
    {
        eventInstance = RuntimeManager.CreateInstance(FishEvent);
        RuntimeManager.AttachInstanceToGameObject(eventInstance, GetComponent<Transform>(), GetComponent<Rigidbody>());
        eventInstance.start();
        eventInstance.release();
    }
}
